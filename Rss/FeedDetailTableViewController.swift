//
//  FeedDetailTableViewController.swift
//  Rss
//
//  Created by zhoubo on 2018/12/29.
//  Copyright © 2018 zhoubo. All rights reserved.
//

import UIKit

class FeedDetailTableViewController: UITableViewController {
    
    fileprivate let text: String
    
    init(text: String) {
        self.text = text
        super.init(style: .grouped)
        
        self.title = "Detail"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.allowsSelection = false
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.numberOfLines = 0
        
        if let data = self.text.data(using: .unicode),
            let attributedText = try? NSAttributedString(data: data,
                                                         options: [.documentType: NSAttributedString.DocumentType.html],
                                                         documentAttributes: nil) {
            cell.textLabel?.attributedText = attributedText
        }
    
        
        return cell
    }
    
}
