//
//  FeedTableViewController.swift
//  Rss
//
//  Created by zhoubo on 2018/12/29.
//  Copyright © 2018 zhoubo. All rights reserved.
//

import UIKit
import RssFramework

class FeedTableViewController: UITableViewController {
    
    var feed: RSSFeed?
    let parser: FeedParser
    
    init(feedURL: URL) {
        parser = FeedParser(URL: feedURL)
        super.init(style: .grouped)
    }
    
    init(feedData: Data) {
        parser = FeedParser(data: feedData)
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Rss Feed"
        
        self.tableView.register(UITableViewCell.self,
                                forCellReuseIdentifier: "Cell")
        parser.parseAsync { [weak self] (result) in
            self?.feed = result.rssFeed
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }

}


// MARK: - Table View Data Source

extension FeedTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 3
        case 1: return self.feed?.items?.count ?? 0
        default: fatalError()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = reusableCell()
        guard let layout = TableViewLayout(indexPath: indexPath) else { fatalError() }
        switch layout {
        case .title:        cell.textLabel?.text = self.feed?.title ?? "[no title]"
        case .link:         cell.textLabel?.text = self.feed?.link ?? "[no link]"
        case .description:  cell.textLabel?.text = self.feed?.description ?? "[no description]"
        case .items:        cell.textLabel?.text = self.feed?.items?[indexPath.row].title ?? "[no title]"
        }
        return cell
    }
    
}

// MARK: - Table View Delegate

extension FeedTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let layout = TableViewLayout(indexPath: indexPath) else { fatalError() }
        switch layout {
        case .title:        self.showDetailViewControllerWithText(self.feed?.title ?? "[no title]")
        case .link:         self.showDetailViewControllerWithText(self.feed?.link ?? "[no link]")
        case .description:  self.showDetailViewControllerWithText(self.feed?.description ?? "[no link]")
        case .items:        self.showDetailViewControllerWithText(self.feed?.items?[indexPath.row].description ?? "[no description]")
        }
    }
    
}

// MARK: - Navigation

extension FeedTableViewController {
    
    func showDetailViewControllerWithText(_ text: String) {
        let viewController = FeedDetailTableViewController(text: text)
        self.show(viewController, sender: self)
    }
    
}

extension FeedTableViewController {
    func reusableCell() -> UITableViewCell {
        let reuseIdentifier = "Cell"
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) {
            return cell
        }
        let cell = UITableViewCell(style: .value1, reuseIdentifier: reuseIdentifier)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
}

extension FeedTableViewController {
    enum TableViewLayout {
        case title, link, description, items
        init?(indexPath: IndexPath) {
            switch indexPath.section {
            case 0:
                switch indexPath.row {
                case 0: self = .title
                case 1: self = .link
                case 2: self = .description
                default: return nil
                }
            case 1: self = .items
            default: return nil
            }
        }
    }
}
