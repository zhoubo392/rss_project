//
//  ViewController.swift
//  Rss
//
//  Created by zhoubo on 2018/12/29.
//  Copyright © 2018 zhoubo. All rights reserved.
//

import UIKit
import RssFramework

class ViewController: UITableViewController {

    let rssCache = FeedDataCache()
    var pasteboardContents: [String] = []
    
    
    init() {
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if rssCache.memoryCacheList.count <= 0,
            let url = URL(string: "https://developer.apple.com/swift/blog/news.rss") {
            rssCache.saveRss(for: url)
        }
        
        self.refreshControl = UIRefreshControl()
        self.title = "Rss List"
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = 44.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(willActive),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rssFeed(notify:)),
                                               name: NSNotification.Name.FeedDataDidParsed,
                                               object: nil)
        
        refreshControl?.addTarget(self, action: #selector(refreshStateChanged),
                                  for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        
        willActive()
    }
    
    // MARK: - Logic
    
    @objc func refreshStateChanged() {
        print(self.refreshControl!.isRefreshing)
        if let rc = self.refreshControl,
            rc.isRefreshing {
            rc.endRefreshing()
            willActive()
        }
    }
    
    @objc func willActive() {
        if let string = UIPasteboard.general.strings?.last,
            let _ = URL(string: string) {
            UIPasteboard.general.strings = nil
            
            if !pasteboardContents.contains(string) {
                pasteboardContents.append(string)
                tableView.reloadSections([1], with: .automatic)
            }
        }
    }
    
    @objc func rssFeed(notify: Notification) {
        if let userinfo = notify.userInfo,
            let path = userinfo[FeedDataCache.FeedDataURLKey] as? String,
            let url = URL(string: path),
            let data = userinfo[FeedDataCache.FeedDataRawDataKey] as? Data {
            rssCache.saveRss(for: url, data: data)
        }
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return rssCache.memoryCacheList.count
        } else {
            return pasteboardContents.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        if indexPath.section == 0 {
            if let url = URL(string: rssCache.memoryCacheList[indexPath.row]) {
                cell.textLabel?.text = url.absoluteString
            }
        } else {
            cell.textLabel?.text = pasteboardContents[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "List"
        } else {
            if (pasteboardContents.count > 0){
                return "Click To Add"
            } else {
                return nil
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let url = URL(string: rssCache.memoryCacheList[indexPath.row]) {
                let feedVC: FeedTableViewController
                if let data = rssCache.rssData(for: url) {
                    feedVC = FeedTableViewController(feedData: data)
                } else {
                    feedVC = FeedTableViewController(feedURL: url)
                }
                navigationController?.pushViewController(feedVC, animated: true)
            }
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if let url = URL(string: self.pasteboardContents[indexPath.row]) {
                    self.rssCache.saveRss(for: url)
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
}

