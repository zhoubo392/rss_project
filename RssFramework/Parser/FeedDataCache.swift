//
//  FeedDataCache.swift
//  RssFramework
//
//  Created by zhoubo on 2018/12/29.
//  Copyright © 2018 zhoubo. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    public static let FeedDataDidParsed = NSNotification.Name.init("FeedDataDidParsed")
}

public class FeedDataCache {

    let cachedListKey = "com.zhou.cache.list.key"
    public static let FeedDataURLKey = "FeedDataURLKey"
    public static let FeedDataRawDataKey = "FeedDataRawDataKey"
    public var memoryCacheList: [String] = []
    var userdefault = UserDefaults.standard
    
    public init() {
        loadData()
    }
    
    private func loadData() {
        guard let array = userdefault.array(forKey: cachedListKey) as? [String]
            else { return }
        self.memoryCacheList = array
    }
    
    public func rssData(for url: URL) -> Data? {
        let urlKey = url.absoluteString
        guard let index = memoryCacheList.firstIndex(of: urlKey)
            else { return nil }
        
        return userdefault.data(forKey: memoryCacheList[index])
    }
    
    public func saveRss(for url: URL, data: Data? = nil) {
        if !memoryCacheList.contains(url.absoluteString) {
            memoryCacheList.append(url.absoluteString)
            userdefault.removeObject(forKey: cachedListKey)
            userdefault.set(memoryCacheList, forKey: cachedListKey)
        }
        if let d = data {
            userdefault.set(d, forKey: url.absoluteString)
        }
    }
    
}
